package radroid.com.spaceinvadeclone.State;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

import radroid.com.spaceinvadeclone.model.EnemyShip;
import radroid.com.spaceinvadeclone.util.DeviceInfo;
import radroid.com.spaceinvadeclone.util.GameController;
import radroid.com.spaceinvadeclone.util.SpawnShip;

/**
 * Created by Radek on 23.10.2015.
 */
public class PlayState implements State {


	private static boolean isInitiated = false;
	private GameController gameController = new GameController();

	@Override
	public void init() {
		gameController.createPlayerShip();

		if (!isInitiated) {
			gameController.spawnFirstWave();
			isInitiated = true;
		}

	}

	@Override
	public void update() {
		gameController.update();
	}

	@Override
	public void draw(Canvas canvas, Paint paint) {
		gameController.drawObjects(canvas, paint);

	}

	@Override
	public boolean onTouchEvents(MotionEvent event) {

		if (event.getPointerCount() >= 2) {

			final int action = event.getActionIndex();

			float multiEventPosX = event.getX(action);

			if (multiEventPosX < DeviceInfo.screenWidth / 2) {
				switch (event.getActionMasked()) {
					case MotionEvent.ACTION_DOWN:
					case MotionEvent.ACTION_POINTER_DOWN:
					case MotionEvent.ACTION_MOVE:
						if (multiEventPosX < DeviceInfo.screenWidth / 4) {
							gameController.playerShip.accelLeft();
							return true;
						} else if (multiEventPosX >= DeviceInfo.screenWidth / 4 && multiEventPosX < DeviceInfo.screenWidth / 2) {
							gameController.playerShip.accelRight();
							return true;
						}
					case MotionEvent.ACTION_UP:
					case MotionEvent.ACTION_POINTER_UP:
					case MotionEvent.ACTION_CANCEL:
						gameController.playerShip.stop();
						return true;
				}
			}

			if (multiEventPosX > DeviceInfo.screenWidth / 2) {
				switch (event.getActionMasked()) {
					case MotionEvent.ACTION_DOWN:
					case MotionEvent.ACTION_POINTER_DOWN:
						gameController.playerShip.fire();
						return true;
				}

			}


		} else {

			float eventPosX = event.getX();

			if (eventPosX < DeviceInfo.screenWidth / 2) {
				switch (event.getActionMasked()) {
					case MotionEvent.ACTION_DOWN:
					case MotionEvent.ACTION_POINTER_DOWN:
					case MotionEvent.ACTION_MOVE:
						if (eventPosX < DeviceInfo.screenWidth / 4) {
							gameController.playerShip.accelLeft();
							return true;
						} else if (eventPosX >= DeviceInfo.screenWidth / 4 && eventPosX < DeviceInfo.screenWidth / 2) {
							gameController.playerShip.accelRight();
							return true;
						}
					case MotionEvent.ACTION_UP:
					case MotionEvent.ACTION_POINTER_UP:
					case MotionEvent.ACTION_CANCEL:
						gameController.playerShip.stop();
						return true;
				}
			}

			if (eventPosX > DeviceInfo.screenWidth / 2) {
				switch (event.getActionMasked()) {
					case MotionEvent.ACTION_DOWN:
					case MotionEvent.ACTION_POINTER_DOWN:
						gameController.playerShip.fire();
						return true;
				}

			}

		}
		return false;
	}
}
