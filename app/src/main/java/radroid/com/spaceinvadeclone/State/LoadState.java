package radroid.com.spaceinvadeclone.State;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;

import radroid.com.spaceinvadeclone.game.GameSurfaceView;
import radroid.com.spaceinvadeclone.util.Assets;
import radroid.com.spaceinvadeclone.util.DeviceInfo;
import radroid.com.spaceinvadeclone.util.StateController;

/**
 * Created by rados on 05.11.2015.
 */
public class LoadState implements State {


	private int posX_TEXT;
	private int posY_TEXT;
	private int posX_PIC;
	private int posY_PIC;

	@Override
	public void init() {
		Assets.load();

		posX_TEXT = DeviceInfo.screenWidth / 2 - Assets.loadScreenText.getWidth() / 2;
		posY_TEXT = DeviceInfo.screenHeight / 4;

		posX_PIC = DeviceInfo.screenWidth / 2 - Assets.loadScreenPicture.getWidth() / 2;
		posY_PIC = DeviceInfo.screenHeight / 2;

	}

	@Override
	public void update() {

	}

	@Override
	public void draw(Canvas canvas, Paint paint) {
		canvas.drawBitmap(Assets.loadScreenText, posX_TEXT, posY_TEXT, paint);
		canvas.drawBitmap(Assets.loadScreenPicture, posX_PIC, posY_PIC, paint);
	}

	@Override
	public boolean onTouchEvents(MotionEvent event) {
		if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
			StateController.setCurrentState(new PlayState());
		}
		return false;
	}
}
