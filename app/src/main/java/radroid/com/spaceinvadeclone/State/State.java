package radroid.com.spaceinvadeclone.State;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

import radroid.com.spaceinvadeclone.game.GameSurfaceView;

/**
 * Created by Radek on 23.10.2015.
 */
public interface State {

	void init();

	void update();

	void draw(Canvas canvas, Paint paint);

	boolean onTouchEvents(MotionEvent event);


}
