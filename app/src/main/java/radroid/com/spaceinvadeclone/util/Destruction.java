package radroid.com.spaceinvadeclone.util;

import android.util.Log;

import java.util.Iterator;
import java.util.Set;

import radroid.com.spaceinvadeclone.model.EnemyShip;
import radroid.com.spaceinvadeclone.model.PlayerBullet;

/**
 * Created by rados on 11.11.2015.
 */
public class Destruction {
	public static final String TAG = Destruction.class.getSimpleName();


	public void destroyEnemyShips(Set<EnemyShip> enemyShips) {
		synchronized (enemyShips) {
			for (Iterator<EnemyShip> iter = enemyShips.iterator(); iter.hasNext(); ) {
				EnemyShip enemyShip = iter.next();
				if (enemyShip.toDestroy) {
					iter.remove();
					Log.d(TAG, "Remove ship");
				}


			}


		}
	}

	public void destroyPlayerBullets(Set<PlayerBullet> playerBullets) {
		synchronized (playerBullets) {

			for (Iterator<PlayerBullet> iter = playerBullets.iterator(); iter.hasNext(); ) {
				PlayerBullet bullet = iter.next();
				if (bullet.posY < 0 || bullet.toDestroy) {
					iter.remove();
				}
			}
		}
	}
}
