package radroid.com.spaceinvadeclone.util;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import java.util.Iterator;
import java.util.Set;

import radroid.com.spaceinvadeclone.model.Destructible;
import radroid.com.spaceinvadeclone.model.EnemyShip;
import radroid.com.spaceinvadeclone.model.PlayerBullet;
import radroid.com.spaceinvadeclone.model.PlayerShip;

/**
 * Created by rados on 10.11.2015.
 */
public class GameController {
	private static final String TAG = GameController.class.getSimpleName();
	public PlayerShip playerShip;
	private Set<EnemyShip> enemyShips;
	private Set<PlayerBullet> playerBullets;
	private Move move = new Move();
	private Destruction destruction = new Destruction();
	private Check check = new Check();
	private DrawGame drawGame = new DrawGame();
	private SpawnShip spawnShip = new SpawnShip();

	public void update() {
		this.playerBullets = PlayerBullet.getPlayerBullets();

		synchronized (enemyShips) {
			synchronized (playerBullets) {

				destroyObjects(enemyShips, playerBullets);
				moveObjects(enemyShips, playerBullets, playerShip);
				check(enemyShips, playerBullets, playerShip);
			}
		}
		if(enemyShips.isEmpty()){
			spawnFirstWave();
		}
	}


	public void destroyObjects(Set<EnemyShip> enemyShips, Set<PlayerBullet> playerBullets) {
		destruction.destroyEnemyShips(enemyShips);
		destruction.destroyPlayerBullets(playerBullets);


	}

	public void moveObjects(Set<EnemyShip> enemyShips, Set<PlayerBullet> playerBullets, PlayerShip playerShip) {
		move.moveEnemyShips(enemyShips);
		move.movePlayerBullets(playerBullets);
		playerShip.move();


	}

	public void check(Set<EnemyShip> enemyShips, Set<PlayerBullet> playerBullets, PlayerShip playerShip) {
		check.checkPlayerHits(enemyShips, playerBullets);


	}


	public void drawObjects(Canvas canvas, Paint paint) {
		// this.enemyShips = spawnShip.getEnemyShips();
		this.playerBullets = PlayerBullet.getPlayerBullets();

		synchronized (enemyShips) {
			synchronized (playerBullets) {

				drawGame.drawPlayerBullets(playerBullets, canvas, paint);
				//  drawEnemyShipBullets();
				drawGame.drawEnemyShips(enemyShips, canvas, paint);
				drawGame.drawPlayerShip(playerShip, canvas, paint);
			}
		}

	}


	public void createPlayerShip() {
		playerShip = new PlayerShip();

	}



	public void spawnFirstWave(){
		enemyShips = spawnShip.spawnEnemyShips();
	}


}
