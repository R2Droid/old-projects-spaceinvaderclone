package radroid.com.spaceinvadeclone.util;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import radroid.com.spaceinvadeclone.model.EnemyShip;

/**
 * Created by rados on 11.11.2015.
 */
public class SpawnShip {
	private Set<EnemyShip> enemyShips = Collections.synchronizedSet(new HashSet<EnemyShip>());

	private static int SPAWN_ROWS = 3;
	private static int SPAWN_COLUMNS = 4;


	public Set<EnemyShip> spawnEnemyShips() {
		for (int y = 0; y < SPAWN_ROWS; y++) {

			for (int x = 0; x < SPAWN_COLUMNS; x++) {
				enemyShips.add(new EnemyShip(x, y));
			}

		}

		return  enemyShips;
	}

	public Set<EnemyShip> getEnemyShips() {
		return enemyShips;
	}

}
