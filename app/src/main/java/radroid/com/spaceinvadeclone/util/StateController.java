package radroid.com.spaceinvadeclone.util;

import radroid.com.spaceinvadeclone.State.State;

/**
 * Created by rados on 10.11.2015.
 */
public class StateController {
	public volatile static State currentState;


	public static void setCurrentState(State state) {
		state.init();
		currentState = state;


	}
}
