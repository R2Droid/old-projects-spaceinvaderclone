package radroid.com.spaceinvadeclone.util;

import android.graphics.Rect;
import android.util.Log;

import java.util.Set;

import radroid.com.spaceinvadeclone.model.EnemyShip;
import radroid.com.spaceinvadeclone.model.PlayerBullet;

/**
 * Created by rados on 11.11.2015.
 */
public class Check {
	private static final String TAG = Check.class.getSimpleName();

	public void checkPlayerHits(Set<EnemyShip> enemyShips, Set<PlayerBullet> playerBullets) {
		for (EnemyShip enemyShip : enemyShips) {


			for (PlayerBullet playerBullet : playerBullets) {
				if (Rect.intersects(playerBullet.getRect(), enemyShip.rect)) {
					enemyShip.onHit();
					playerBullet.onHit();
					Log.d(TAG, "HIT");
				}

			}
		}

	}
}
