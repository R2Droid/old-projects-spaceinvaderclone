package radroid.com.spaceinvadeclone.util;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.io.InputStream;

import radroid.com.spaceinvadeclone.game.MainActivity;

/**
 * Created by rados on 04.11.2015.
 */
public class Assets {

	public static Bitmap playerShip;
	public static Bitmap loadScreenText;
	public static Bitmap loadScreenPicture;
	public static Bitmap enemyShip;

	public static void load() {
		playerShip = loadBitmap("player_ship_s.png", false);
		enemyShip = loadBitmap("enemy_ship_saucer.png", false);
		loadScreenText = loadBitmap("loading_screen_text.png", false);
		loadScreenPicture = loadBitmap("loading_screen_picture.png", false);

	}


	private static Bitmap loadBitmap(String filename, boolean isTransparent) {
		InputStream inputStream = null;
		Bitmap bitmap = null;

		try {
			inputStream = MainActivity.assetManager.open(filename);

			BitmapFactory.Options options = new BitmapFactory.Options();

			if (isTransparent) {
				options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			} else {
				options.inPreferredConfig = Bitmap.Config.RGB_565;
			}
			bitmap = BitmapFactory.decodeStream(inputStream, null, options);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {	/* return null */ }
			}
		}

		return bitmap;
	}

}
