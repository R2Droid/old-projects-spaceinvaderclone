package radroid.com.spaceinvadeclone.util;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Set;

import radroid.com.spaceinvadeclone.model.EnemyShip;
import radroid.com.spaceinvadeclone.model.PlayerBullet;
import radroid.com.spaceinvadeclone.model.PlayerShip;

/**
 * Created by rados on 11.11.2015.
 */
public class DrawGame {
	public void drawPlayerBullets(Set<PlayerBullet> playerBullets, Canvas canvas, Paint paint) {
		synchronized (playerBullets) {
			for (PlayerBullet bullet : playerBullets) {
				canvas.drawRect(bullet.rect, paint);
			}

		}


	}

	public void drawEnemyShipsBullets(Canvas canvas, Paint paint) {
		// TODO

	}

	public void drawEnemyShips(Set<EnemyShip> enemyShips, Canvas canvas, Paint paint) {
		synchronized (enemyShips) {
			for (EnemyShip enemyShip : enemyShips) {
				canvas.drawBitmap(Assets.enemyShip, enemyShip.posX, enemyShip.posY, paint);
			}

		}

	}


	public void drawPlayerShip(PlayerShip playerShip, Canvas canvas, Paint paint) {
		canvas.drawBitmap(Assets.playerShip, playerShip.getPosX(), playerShip.getPosY(), paint);


	}


}
