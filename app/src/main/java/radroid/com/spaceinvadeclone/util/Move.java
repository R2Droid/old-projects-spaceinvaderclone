package radroid.com.spaceinvadeclone.util;

import java.util.Set;

import radroid.com.spaceinvadeclone.model.EnemyShip;
import radroid.com.spaceinvadeclone.model.PlayerBullet;
import radroid.com.spaceinvadeclone.model.PlayerShip;

/**
 * Created by rados on 11.11.2015.
 */
public class Move {

	public void moveEnemyShips(Set<EnemyShip> enemyShips) {

		for (EnemyShip enemyShip : enemyShips) {
			enemyShip.move();
		}


	}

	public void movePlayerBullets(Set<PlayerBullet> playerBullets) {

		for (PlayerBullet bullet : playerBullets) {
			bullet.move();
			bullet.rect.offsetTo(bullet.posX, bullet.posY);
		}


	}

}
