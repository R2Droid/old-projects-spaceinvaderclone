package radroid.com.spaceinvadeclone.model;

/**
 * Created by rados on 10.11.2015.
 */
public interface Movable {

	void move();

	void stop();
}
