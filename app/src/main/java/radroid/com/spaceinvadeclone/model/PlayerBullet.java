package radroid.com.spaceinvadeclone.model;

import android.graphics.Rect;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import radroid.com.spaceinvadeclone.util.Assets;

/**
 * Created by Radek on 23.10.2015.
 */
public class PlayerBullet implements Movable, Destructible {
	private final int PLAYER_BULLET_WIDTH = 6;
	private final int PLAYER_BULLET_HEIGHT = 20;
	private final int BULLET_POS_X_CORRECTION = PlayerShip.PLAYER_WIDTH / 2 - PLAYER_BULLET_WIDTH / 2;
	private final int BULLET_POS_Y_CORRECTION = Assets.playerShip.getHeight() / 8;


	public int posX;
	public int posY;
	private int moveSpeed = 15;
	public boolean toDestroy = false;

	public Rect rect;

	private static  Set<PlayerBullet> playerBullets = Collections.synchronizedSet(new HashSet<PlayerBullet>());

	PlayerBullet(int posX, int posY) {
		this.posX = posX + BULLET_POS_X_CORRECTION;
		this.posY = posY + BULLET_POS_Y_CORRECTION;

		this.rect = new Rect(this.posX, posY, this.posX + PLAYER_BULLET_WIDTH, posY + PLAYER_BULLET_HEIGHT);
		playerBullets.add(this);
	}


	public void setToDestroy() {
		toDestroy = true;
	}


	public void onHit() {
		setToDestroy();
	}

	public void move() {
		posY -= moveSpeed;
	}

	public void stop() {
		moveSpeed = 0;
	}


	public static Set<PlayerBullet> getPlayerBullets() {
		return playerBullets;
	}

	public Rect getRect() {
		return rect;
	}

}
