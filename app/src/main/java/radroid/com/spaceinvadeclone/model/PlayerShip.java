package radroid.com.spaceinvadeclone.model;

import android.graphics.Rect;

import radroid.com.spaceinvadeclone.util.Assets;
import radroid.com.spaceinvadeclone.util.DeviceInfo;

/**
 * Created by rados on 21.10.2015.
 */
public class PlayerShip implements Movable, Destructible {

	static final int PLAYER_WIDTH = Assets.playerShip.getWidth();
	private final int PLAYER_HEIGHT = Assets.playerShip.getHeight();
	private final int MOVE_SPEED = 10;

	private int velX = 0;


	private int posX;
	private int posY;
	private Rect rectangle;
	public int hp = 100;
	public boolean toDestroy = false;

	public PlayerShip() {


		this.posX = DeviceInfo.screenWidth / 2 - PLAYER_WIDTH / 2;
		this.posY = DeviceInfo.screenHeight - PLAYER_HEIGHT / 2 - 300;
		this.rectangle = new Rect(
				posX,
				posY,
				posX + PLAYER_WIDTH,
				posY + PLAYER_HEIGHT

		);

	}


	public void move() {
		posX += velX;
		if (posX < 0) {
			posX = 0;
		} else if (posX > DeviceInfo.screenWidth - PLAYER_WIDTH) {
			posX = DeviceInfo.screenWidth - PLAYER_WIDTH;
		}
		rectangle.offsetTo(posX, posY);


	}

	public void accelLeft() {
		velX = -MOVE_SPEED;
	}

	public void accelRight() {
		velX = MOVE_SPEED;
	}

	public void stop() {
		velX = 0;
	}

	public void fire() {
		new PlayerBullet(this.posX, posY);

	}

	public void onHit() {
		hp--;
		if (hp <= 0) {
			setToDestroy();
		}

	}

	public void setToDestroy() {
		toDestroy = true;
	}

	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}
}

