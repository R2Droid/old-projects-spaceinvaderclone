package radroid.com.spaceinvadeclone.model;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;


import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import radroid.com.spaceinvadeclone.util.Assets;

/**
 * Created by Radek on 02.11.2015.
 */
public class EnemyShip implements Movable, Destructible {
	private static final String TAG = EnemyShip.class.getSimpleName();
	private int MOVE_SPEED = 1;
	private final int MOVE_AMPLITUDE = 100;




	public int posX = 100;
	public int posY = 100;
	public Rect rect;
	private int distanceTraveled;
	private int direction = 1;

	private int hits = 0;
	public boolean toDestroy = false;




	public EnemyShip(int xCorrection, int yCorrection) {
		posX += 200 * xCorrection;
		posY += 150 * yCorrection;
		rect = new Rect(
				posX,
				posY,
				posX + Assets.enemyShip.getWidth(),
				posY + Assets.enemyShip.getHeight());
	}





	public void move() {
		posX += MOVE_SPEED * direction;
		distanceTraveled += MOVE_SPEED;
		if (distanceTraveled > MOVE_AMPLITUDE) {
			direction = direction * -1;
			distanceTraveled = 0;
		}
		rect.offsetTo(posX, posY);
	}

	public void stop() {
		MOVE_SPEED = 0;
	}

	void fire() {

	}

	public void onHit() {
		hits++;
		if (hits >= 2) {
			setToDestroy();
		}
	}

	public void setToDestroy() {
		toDestroy = true;
	}

}
