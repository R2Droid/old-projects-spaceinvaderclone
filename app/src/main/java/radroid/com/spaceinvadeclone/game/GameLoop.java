package radroid.com.spaceinvadeclone.game;

import android.content.Context;
import android.graphics.Canvas;
import android.os.SystemClock;
import android.util.Log;
import android.view.SurfaceHolder;

/**
 * Created by rados on 21.10.2015.
 */
public class GameLoop implements Runnable {
	private static final String TAG = GameSurfaceView.class.getSimpleName();

	private static final int TARGET_FPS = 60;
	private static  final long  FRAME_TIME = 1000/TARGET_FPS;
	private static final int MAX_FRAMES_SKIPPED = 5;
	private static final int WAIT_BEFORE_START = 1000;

	volatile boolean running = false;

	GameSurfaceView gameSurfaceView;
	Thread renderThread = null;
	SurfaceHolder surfaceHolder;


	public GameLoop(GameSurfaceView gameSurfaceView){
		super();
		this.gameSurfaceView = gameSurfaceView;
		this.surfaceHolder = gameSurfaceView.getHolder();

	}

	@Override
	public void run() {
		Canvas canvas = null;
		long beginTime;
		long overTime = 0L;
		long endTime = 0L;
		long totalTime = 0L; // DEBUG
		long sleepTime = 0L;
		int skippedFrames = 0;
		int averageFPS = 0; // DEBUG
		long diffTime;
		int behindSchedule = 0;

		try {
			Thread.sleep(WAIT_BEFORE_START);
		}catch (InterruptedException e){

		}
		while(running){

			if(!surfaceHolder.getSurface().isValid()){
				Log.v(TAG, "SurfaceHolder not valid !");
								continue;
			}
			try {
				canvas = null;
				skippedFrames = 0;
				beginTime = SystemClock.uptimeMillis();
				canvas = surfaceHolder.lockCanvas();

				gameSurfaceView.update();

				gameSurfaceView.onDraw(canvas);

				endTime = SystemClock.uptimeMillis();
				sleepTime = (FRAME_TIME - (endTime - beginTime)) - overTime;

				if(sleepTime > 0){
					Thread.sleep(sleepTime);
					//Log.v(TAG, "SLEEP FOR: " + sleepTime);
					overTime = (SystemClock.uptimeMillis()- endTime) - sleepTime;
					// Log.v(TAG, "Over time: " + overTime);
				}else {
					overTime = 0L;
					behindSchedule -= sleepTime;


				while(behindSchedule > FRAME_TIME && skippedFrames < MAX_FRAMES_SKIPPED){
					// update
					Log.v(TAG, "SKIP");
					skippedFrames++;
					behindSchedule -= FRAME_TIME;
				}
				}
				diffTime = SystemClock.uptimeMillis() - beginTime;
				averageFPS++;
				totalTime = totalTime + diffTime;

				if(totalTime > 1000){
					Log.d(TAG, "Average FPS: " + averageFPS);
					averageFPS = 0;
					totalTime = 0;
				}


			}catch (InterruptedException e){
				e.printStackTrace();
			}finally {
				if(canvas != null) {
					surfaceHolder.unlockCanvasAndPost(canvas);
				}
			}


		}

	}
	public void resume(){
		running = true;
		renderThread = new Thread(this);
		renderThread.start();
	}

	public void pause(){
		running = false;
		while(true) {
			try {
				renderThread.join();
				return;
			} catch (InterruptedException e) {

			}
		}

	}
}
