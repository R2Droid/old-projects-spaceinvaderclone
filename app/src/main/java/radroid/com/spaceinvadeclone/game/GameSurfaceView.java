package radroid.com.spaceinvadeclone.game;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.DisplayMetrics;

import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


import radroid.com.spaceinvadeclone.State.LoadState;
import radroid.com.spaceinvadeclone.State.State;
import radroid.com.spaceinvadeclone.util.DeviceInfo;
import radroid.com.spaceinvadeclone.util.StateController;

/**
 * Created by rados on 20.10.2015.
 */
public class GameSurfaceView extends SurfaceView {
	private static final String TAG = GameSurfaceView.class.getSimpleName();
	private DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();

	private final int SCREEN_HEIGHT = displayMetrics.heightPixels;
	private final int SCREEN_WIDTH = displayMetrics.widthPixels;

	SurfaceHolder surfaceHolder;
	Paint paint = new Paint();


	public GameSurfaceView(Context context) {
		super(context);
		DeviceInfo.initialize(this);
		surfaceHolder = getHolder();
		StateController.setCurrentState(new LoadState());
	}


	public void update() {
		StateController.currentState.update();


	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawColor(0, PorterDuff.Mode.CLEAR);
		paint.setColor(Color.WHITE);
		StateController.currentState.draw(canvas, paint);


	}


	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return StateController.currentState.onTouchEvents(event);
	}

	public int getSCREEN_HEIGHT() {
		return SCREEN_HEIGHT;
	}

	public int getSCREEN_WIDTH() {
		return SCREEN_WIDTH;
	}


}
