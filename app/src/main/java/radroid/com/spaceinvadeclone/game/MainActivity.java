package radroid.com.spaceinvadeclone.game;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.util.Random;

public class MainActivity extends AppCompatActivity {


	private static final String TAG = MainActivity.class.getSimpleName();
	GameLoop gameLoop;
	GameSurfaceView gameSurfaceView;
	public static AssetManager assetManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		assetManager = getAssets();
		gameSurfaceView = new GameSurfaceView(this);
		gameLoop = new GameLoop(gameSurfaceView);


		setContentView(gameSurfaceView);
	}

	@Override
	protected void onResume() {
		super.onResume();
		gameLoop.resume();
		Log.v(TAG, "RESUMING");
	}

	@Override
	protected void onPause() {
		super.onPause();
		gameLoop.pause();
		Log.v(TAG, "PAUSING");

	}


}
